DEBUG = True
BUILD_DIR = dist

SSH_HOSTNAME = manufactura
SSH_DIR = /home/manufactura/apps/static-rastreamento/
STAGING_DIR = /home/manufactura/apps/static-rastreamento/staging/
SSH_PATH = $(SSH_HOSTNAME):$(SSH_DIR)
STAGING_PATH = $(SSH_HOSTNAME):$(STAGING_DIR)

SASS_FILES_DIR = css
JS_FILES_DIR = js
FONTS_DIR = fonts
IMAGES_DIR = images
CONTENT_IMAGES_DIR = images
JS_VENDOR_FILES_DIR = js/vendor
TARGET_CSS_DIR = $(BUILD_DIR)/css
TARGET_JS_DIR = $(BUILD_DIR)/js
TARGET_FONTS_DIR = $(BUILD_DIR)/fonts
TARGET_IMAGES_DIR = $(BUILD_DIR)/images

# CSS via Sass
SASS_FILES = $(wildcard $(SASS_FILES_DIR)/*.scss)
CSS_FILES=$(patsubst $(SASS_FILES_DIR)/%.scss,$(TARGET_CSS_DIR)/%.css,$(SASS_FILES))
SASS=./lib/sass
ifeq ($(DEBUG),True)
    SASS_FLAGS = --style expanded
else
    SASS_FLAGS = --style compressed --quiet
endif

# helper python commands
ACTIVATE=. `pwd`/.env/bin/activate

build: dir css js fonts html images

dir:
	mkdir -p $(BUILD_DIR) $(TARGET_CSS_DIR) $(TARGET_JS_DIR) $(TARGET_FONTS_DIR) $(TARGET_IMAGES_DIR)

# css: $(CSS_FILES)
# $(TARGET_CSS_DIR)/%.css: $(SASS_FILES_DIR)/%.scss
	# $(SASS) $(SASS_FLAGS) $< > $@

css:
	cp $(SASS_FILES_DIR)/*.css $(TARGET_CSS_DIR)
js:
	# cp $(JS_FILES_DIR)/*.js $(TARGET_JS_DIR)
	# cp $(JS_VENDOR_FILES_DIR)/*.js $(TARGET_JS_DIR)

fonts:
	cp $(FONTS_DIR)/* $(TARGET_FONTS_DIR)

images:
	cp $(IMAGES_DIR)/* $(TARGET_IMAGES_DIR)
	cp $(CONTENT_IMAGES_DIR)/* $(TARGET_IMAGES_DIR) -r

html:
	cp *.html $(BUILD_DIR)
	# copiar páginas para diretórios
	bash generate.sh

serve:
	make -j2 runserver watch
runserver:
	cd dist; python3 -m http.server 8000
watch:
	watch make
clean:
	rm -fr $(BUILD_DIR)
install:
	virtualenv .env --python=/usr/bin/python3 
	$(ACTIVATE); pip install -r requirements.txt


deploy: build
	rsync --compress --checksum --progress --recursive --update dist/ $(STAGING_PATH)
dry-deploy: build
	rsync --dry-run --compress --checksum --progress --recursive --update dist/ $(STAGING_PATH)
live-deploy: build
	rsync --compress --checksum --progress --recursive --update dist/ $(SSH_PATH)
live-dry-deploy: build
	rsync --dry-run --compress --checksum --progress --recursive --update dist/ $(SSH_PATH)

.PHONY: clean install watch serve html css js dir fonts images
